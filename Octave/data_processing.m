filename_pre = "01133_532019";
file_extension = "csv";

filename = sprintf("%s.%s", filename_pre, file_extension)

data = csvread(filename);
disp("data size: "), disp(size(data));

time_stamp = data(:, 1); 
sensor_data = data(:, [2, 3, 4]);

time_differ = time_stamp(2) - time_stamp(1) # time different in second
four_minutes_window = round(4 * 60 / time_differ)

#sensor_data = medfilt1(sensor_data,four_minutes_window);
#sensor_data = sgolayfilt(sensor_data, round(1/time_differ));


[nr, nc] = size(sensor_data);

function retval = median_filter1(v, w)
  [nr, nc] = size(v);
  retval = zeros(nr, nc);
  half_w = round(w / 2);
  padded_data = [zeros(half_w, nc); v; zeros(half_w, nc)];
  for i=1:nr
    window_data = padded_data(i : i + w, :);
    window_data = sortrows(window_data);
    size_window = rows(window_data);
    median_index = round(size_window / 2);
    if (mod(size_window, 2) == 1)
      retval(i, :) = window_data(median_index + 1);
    else
      retval(i, :) = (window_data(median_index) + window_data(median_index + 1)) / 2;
    endif
    #retval(i, :) = median(window_data);
  endfor
endfunction

function retval = magnitude(v)
  retval = 0;
  if (nargin != 1)
    usage ("avg (vector)");
   endif
   if (isvector(v))
     sv = v.*v;
     dp = sum(sv);
     retval = sqrt(dp);
   else
     error("avg: expected vector argument");
   endif  
endfunction

sensor_magnitude = zeros(nr, 1);

disp("begin calculate magnitude of sensor value");

for i = 1:nr 
  sensor_magnitude(i, 1) = magnitude(sensor_data(i,:));
endfor

#output_filename = sprintf("%s_magnituded.%s", filename_pre, file_extension);
#output_data = [time_stamp sensor_magnitude];
#csvwrite(output_filename, sensor_magnitude);

#sensor_magnitude = medfilt1(sensor_magnitude, four_minutes_window);
#sensor_magnitude = sgolayfilt(sensor_magnitude, round(1/time_differ));

sensor_magnitude = median_filter1(sensor_magnitude, four_minutes_window);

#csvwrite("our_medfilt1.csv", our_sensor_magnitude);
#csvwrite("built_in_medfilt1.csv", sensor_magnitude);

max_value = max(sensor_magnitude)
min_value = min(sensor_magnitude)
range_value = range(sensor_magnitude)

normalized_data = arrayfun(@(x) (x-min_value)/range_value, sensor_magnitude);

output_filename = sprintf("%s_magnitude_normalize.%s", filename_pre, file_extension)
output_data = [time_stamp normalized_data];
csvwrite(output_filename, output_data);

clf;
plot(normalized_data);
xlabel("Time");
ylabel("Normalized Magnitude");
title("Sensor data over time");
