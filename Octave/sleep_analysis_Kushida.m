filename_pre = "01133_532019";
file_extension = "csv";

input_filename = sprintf("%s_magnituded.%s", filename_pre, file_extension)

data = csvread(input_filename);

time_stamp = data(:, 1); 
magnitude_data = data(:, 2);

mean_value = mean(magnitude_data);
#std_deviation = std(magnitude_data);
#threshold = (mean_value + std_deviation) / 2;
most_appearance = mode(magnitude_data)
threshold = most_appearance;


data_len = rows(magnitude_data)
modified_data = zeros(data_len, 1);

for i=1:data_len
    if (i > 4 && i < data_len - 4)
        modified_data(i) = 0.04 * magnitude_data(i - 4) + 0.04 * magnitude_data(i - 3) + 0.2 * magnitude_data(i - 2) + 0.2 * magnitude_data(i - 1) + 2 * magnitude_data(i) + 0.2 * magnitude_data(i + 1) + 0.2 * magnitude_data(i + 2) + 0.04 * magnitude_data(i + 3) + 0.04 * magnitude_data(i + 4);
    else
        modified_data(i) = magnitude_data(i);
    endif
endfor

state = zeros(data_len, 1);
for i=1:data_len
    if (modified_data(i) >= threshold)
        state(i) = 1;
    else
        state(i) = 2;
    endif
endfor

output_state = sprintf("%s_state_kushida.csv", filename_pre)
csvwrite(output_state, state);

