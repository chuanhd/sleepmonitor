#arg_list = argv();
#filename = arg_list{1};
#update_interval = arg_list{2}; # update frequency per seconds

#one_cycle_rows = 90 * 60 * 10;
#four_minutes = 4 * 60 * 10;
#begin_time = one_cycle_rows * 2;
#data = csvread("234812_2822019.csv", [begin_time, 0, begin_time + four_minutes, 3]);
#disp("data size: "), disp(size(data));

  
filename_pre = "01133_532019";
file_extension = "csv";

normalized_filename = sprintf("%s_magnitude_normalize.%s", filename_pre, file_extension)

data = csvread(normalized_filename);

time_stamp = data(:, 1); 
normalized_data = data(:, 2);

time_differ = time_stamp(2) - time_stamp(1) # time different in second
four_minutes = round(4 * 60 / time_differ)
data_len = rows(normalized_data);
count = 0;

num_windows = ceil(data_len / four_minutes);

state = zeros(num_windows, 1);
disp("state windows: "), disp(size(state));

begin_offset = 0;

for i = 1:num_windows
  begin_offset = (i - 1) * four_minutes + 1;
  end_offset = begin_offset + four_minutes;
  if (end_offset > rows(normalized_data))
    end_offset = rows(normalized_data);
  endif
  windows_data = normalized_data(begin_offset : end_offset, 1);
  mean_value = mean(windows_data);
  std_deviation = std(windows_data);
  #threshold = (mean_value + std_deviation) / 2
  threshold = mean_value
  window_size = end_offset - begin_offset;
  for j = 1:window_size
    if (windows_data(j) > threshold)
      count = count + 1;
    endif
  endfor

  count
  window_size

  # 1: Deep sleep
  # 2: Light sleep
  # 3: Wake

  if (count < 0.45 * window_size)
    state(i) = 1;
  elseif (count < 0.67 * window_size) 
    state(i) = 2;
  else
    state(i) = 3;
  endif

  count = 0;

endfor

output_state = sprintf("%s_state.csv", filename_pre)
csvwrite(output_state, state);

clf;
stem(state);
xlabel("Time");
ylabel("State");
title("Wake/Sleep state over time");

#{
clf;
line ([0 rows(normalized_data)], [threshold threshold], "linestyle", "-", "color", "b");
hold on;
plot(state);
xlabel("Time");
ylabel("Magnitude");
title("Sensor data over time");
#}
