//
//  DataProcessingHelper.swift
//  InspireSleep
//
//  Created by Chuan Ho Danh on 3/13/19.
//  Copyright © 2019 Kappboom. All rights reserved.
//

import Foundation
import Surge
import RealmSwift

class DataProcessingHelper {
    init() {
        
    }
    
    static func processData(withCompletionHandler completionHandler: @escaping (ThreadSafeReference<SleepInfoModel>?) -> Void) {
        
        NSLog("Begin processing data...")
        
        // Read filterred csv data
        if let dataPath = Bundle.main.path(forResource: "1552672160.973779_filterred", ofType: "csv") {
            let url = URL(fileURLWithPath: dataPath)
            do {
                let data = try Data(contentsOf: url)
                let content = String(data: data, encoding: String.Encoding.utf8)
                let parsedCSV: [Double] = content!.components(separatedBy: "\n").filter({ (line) -> Bool in
                    return line.count > 1
                }).map { (line) -> Double in
                    return Double(line)!
//                    return line.components(separatedBy: ",").map({ (item) -> Double in
//                        return Double(item)!
//                    })
                }
                
                // Normalized filterred data
                let normalized = DataProcessingHelper.normalized(data: parsedCSV)
                let states = DataProcessingHelper.getSleepState(withData: normalized, withSleepWindowSize: 2400)
                
                let sleepDuration = normalized.count / 10 // Sleep duration in seconds
                
                let info = SleepInfoModel()
                info.beginSleepTime = Date()
                info.endSleepTime = info.beginSleepTime!.addingTimeInterval(Double(sleepDuration))
                states.forEach { (state) in
                    info.sleepStates.append(state)
                }
                
                let realm = try! Realm()
                try! realm.write {
                    realm.deleteAll() // Clear database
                    realm.add(info)
                }
                
                debugPrint("Finish get sleep state")
                debugPrint(states)
                
                let infoRef = ThreadSafeReference(to: info)
                completionHandler(infoRef)
                
            } catch {
                debugPrint("Failed to parse csv")
            }
        }
        
        
    }
    
    static func medianFilter(data: [Double], windowSize: Int) -> [Double] {
        guard windowSize <= data.count else { fatalError("Window size must be less than data size")}
        let halfWindowSize = Int(Double(windowSize / 2).rounded(.down))
        let paddingData = Array(repeating: Double(0), count: halfWindowSize)
        var paddedData = data
        paddedData.insert(contentsOf: paddingData, at: 0)
        paddedData.append(contentsOf: paddingData)
        
        NSLog("Finish appending data")
        
        var result = Array<Double>(repeating: 0, count: data.count)
        
        let taskGroup = DispatchGroup()
        
        DispatchQueue.concurrentPerform(iterations: data.count) { (i) in
            taskGroup.enter()
            
            var windowData = Array(paddedData[i..<i+windowSize])
            windowData.sort()
            let medianIndex = Int(Double(windowData.count / 2).rounded(.down))
            if windowData.count % 2 == 1 {
                result[i] = (windowData[medianIndex])
            } else {
                result[i] = (windowData[medianIndex] + windowData[medianIndex + 1]) * 0.5
            }
            
            taskGroup.leave()
        }
        
        taskGroup.notify(queue: DispatchQueue.main) {
            NSLog("All tasks done")
        }
        
        return result
    }
    
    static func medianFilter(paddedData: [Double], windowSize: Int, index : Int) -> Double {
        var result : Double = 0.0
        var windowData = Array(paddedData[index..<index + windowSize])
        windowData.sort()
        let medianIndex = Int(Double(windowData.count / 2).rounded(.down))
        if windowData.count % 2 == 1 {
            result = (windowData[medianIndex])
        } else {
            result = (windowData[medianIndex] + windowData[medianIndex + 1]) * 0.5
        }
    
        return result
    }
    
    static func medianFilter(withPaddedData paddedData: [Double],withWindowSize windowSize: Int,withRange range: Range<Int>, withCompletionHandler completionHandler: @escaping ([Double]) -> Void) {
        let taskGroup = DispatchGroup()
        
        var medians : [Double] = []
        
        for i in range {
            taskGroup.enter()
            
            let result = medianFilter(paddedData: paddedData, windowSize: windowSize, index: i)
            medians.append(result)
            
            taskGroup.leave()
        }
        
        taskGroup.notify(queue: DispatchQueue.main) {
            NSLog("All tasks done")
            completionHandler(medians)
        }
    }
    
    static func normalized(data: [Double]) -> [Double] {
        let min = Surge.min(data)
        let max = Surge.max(data)
        let range : Double = max - min
        var normalizedData = Surge.sub(data, y: Array(repeating: min, count: data.count))
        normalizedData = Surge.div(normalizedData, y: Array(repeating: range, count: data.count))
        
        return normalizedData
    }
    
    static func getSleepState(withData normalizeData: [Double], withSleepWindowSize windowSize: Int) -> [Int] {
        let numWindows = Int((Double(normalizeData.count) / Double(windowSize)).rounded(.up))
        var states = Array(repeating: 0, count: numWindows)
        for i in 0..<numWindows {
            let beginOffset = i * windowSize
            var endOffset = beginOffset + windowSize
            if endOffset > normalizeData.count {
               endOffset = normalizeData.count
            }
            let windowData = Array(normalizeData[beginOffset..<endOffset])
//            let threshold = Surge.sum(windowData) / Double(windowData.count)
            let threshold = Surge.mean(windowData)
            var count = 0
            for value in windowData {
                if value > threshold {
                    count += 1
                }
            }
            
            debugPrint("threshold: \(threshold)")
            debugPrint("count: \(count)")
            
            let dataCount = Double(windowSize)
            if count < Int((0.45 * dataCount).rounded(.down)) {
                states[i] = 1
            } else if count < Int((0.67 * dataCount).rounded(.down)) {
                states[i] = 2
            } else {
                states[i] = 3
            }
        }
        
        return states
    }
}

extension Array where Element : Collection {
    func getColumn(column : Element.Index) -> [ Element.Iterator.Element ] {
        return self.map { $0[ column ] }
    }
}
