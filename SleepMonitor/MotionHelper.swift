//
//  MotionHelper.swift
//  SleepMonitor
//
//  Created by Chuan Ho Danh on 2/26/19.
//  Copyright © 2019 Kappboom. All rights reserved.
//

import Foundation
import CoreMotion
import UIKit
import AVFoundation

class MotionHelper {
    static let shared = MotionHelper()
    
    private lazy var motionManager = CMMotionManager()
    var updateInterval = TimeInterval(1/10)
    var stopped = false
    
    private var player : AVPlayer?
    
    private init() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.mixWithOthers)
        } catch(let e) {
            debugPrint("Failed to set audio session category: \(e)")
        }
        
    }
    
    private func playSilentAudio() {
        let item = AVPlayerItem(url: Bundle.main.url(forResource: "silence", withExtension: "mp3")!)
        player = AVPlayer(playerItem: item)
        player?.actionAtItemEnd = .none
        player?.play()
    }
    
    func startMotionUpdates(withCompletionHandler completionHandler: @escaping ([Double]) -> Void) {
        guard motionManager.isAccelerometerAvailable else { return }
        
        self.stopped = false
        self.playSilentAudio()
        
        motionManager.accelerometerUpdateInterval = updateInterval
        motionManager.startAccelerometerUpdates(to: .main) {[weak self] (accelerometerData, error) in
            guard let self = self else { return }
            guard !self.stopped else { return }
            guard let accelerometerData = accelerometerData else { return }
            let acceleration = [accelerometerData.acceleration.x, accelerometerData.acceleration.y, accelerometerData.acceleration.z]
            completionHandler(acceleration)
        }
    }
    
    func stopMotionUpdates() {
        if self.motionManager.isAccelerometerActive {
            self.motionManager.stopAccelerometerUpdates()
            self.stopped = true
        }
    }
    
    func isAccelerometerUpdating() -> Bool {
        return self.motionManager.isAccelerometerActive
    }
    
}
