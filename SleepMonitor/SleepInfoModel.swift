//
//  SleepInfoModel.swift
//  InspireSleep
//
//  Created by ChuanHD on 3/17/19.
//  Copyright © 2019 Kappboom. All rights reserved.
//

import Foundation
import RealmSwift

class SleepInfoModel : Object {
    @objc dynamic var beginSleepTime : Date?
    @objc dynamic var endSleepTime : Date?
    @objc dynamic var filterredDataLog : String?
    let sleepStates = List<Int>()
}
