//
//  ViewController.swift
//  SleepMonitor
//
//  Created by Chuan Ho Danh on 2/26/19.
//  Copyright © 2019 Kappboom. All rights reserved.
//

import UIKit
import Foundation
import Surge
import Charts
import RealmSwift

class ViewController: UIViewController {

    private var log : String = ""
    @IBOutlet weak var lblUpdateInterval: UILabel!
    @IBOutlet weak var txtLog: UILabel!
    @IBOutlet weak var viewCharts: BarChartView!
    
    private var dataCounter = 0
    private var data = [Double]()
    private var filterredData = [Double]()
    private var windowSize = 2400
    private var filterredFlags = NSMutableOrderedSet()
    private var fileName : String = ""
    private var maxValue : Double = -1
    private var minValue : Double = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        MotionHelper.shared.updateInterval = TimeInterval(0.1)
    }

    @IBAction func btnStartUpdatePressed(_ sender: Any) {
        
        do {
            
            windowSize = Int((4 * 60 / MotionHelper.shared.updateInterval).rounded(.down))
            let halfWindowSize = windowSize / 2
            debugPrint("windowSize: \(windowSize)")
            
            // Append zero pads to the beginning of data
            let paddingData = Array(repeating: Double(0), count: halfWindowSize)
            data.insert(contentsOf: paddingData, at: 0)
            
            fileName = "\(Date().timeIntervalSince1970)"
            let logFile = try logFileDest(fileName: "\(fileName).csv")
            MotionHelper.shared.startMotionUpdates(withCompletionHandler: {[weak self] (acceleration) in
                guard let self = self else { return }
                let newLog = "\(Date().timeIntervalSince1970),\(acceleration[0]), \(acceleration[1]), \(acceleration[2])" + "\n"
                self.txtLog.text = newLog
                do {
                    try logFile.append(string: newLog)
                } catch {
                    debugPrint("Failed to append log to log file. Stop accelerometer update")
                    MotionHelper.shared.stopMotionUpdates()
                }
                let magnitude = sqrt(pow(acceleration[0], 2) + pow(acceleration[1], 2) + pow(acceleration[2], 2))
                self.data.append(magnitude)
                self.filterredData.append(0)
                
                if self.data.count >= self.windowSize { // We can begin to apply filter to sensor data
                    let index = self.data.count - self.windowSize
                    self.processData(atIndex: index)
                }
                
            })
        } catch {
            debugPrint("Failed to created log file. Stop accelerometer update")
            MotionHelper.shared.stopMotionUpdates()
        }
        
        
    }
    
    @IBAction func btnStopUpdatePressed(_ sender: Any) {
        MotionHelper.shared.stopMotionUpdates()
        
//        debugPrint("Unfilterred data count: \(self.filterredData.count - self.filterredFlags.count)")
//        debugPrint("Filterred data: \(self.filterredFlags)")
        
        // We will continue computing unfilterred data
        NSLog("Continue computing remaining data...")
        
        // Append zero pads to the end of data
        let halfWindowSize = windowSize / 2
        let paddingData = Array(repeating: Double(0), count: halfWindowSize)
        data.append(contentsOf: paddingData)
        
        let beginIndex = self.filterredFlags.count
        let effectedRange = beginIndex..<self.filterredData.count
        DataProcessingHelper.medianFilter(withPaddedData: data, withWindowSize: windowSize, withRange: effectedRange) {[weak self] (medians) in
            guard let self = self else { return }
            self.filterredData.replaceSubrange(effectedRange, with: medians)
            
            let normalizedData = DataProcessingHelper.normalized(data: self.filterredData)
            let states = DataProcessingHelper.getSleepState(withData: normalizedData, withSleepWindowSize: self.windowSize)
            
            NSLog("Finish computing remaining data...: \(states)")
            
            do {
                let logFile = try self.logFileDest(fileName: "\(self.fileName)_filterred.csv")
                let filterredLog = self.filterredData.map{ String($0) }.joined(separator: "\n")
                try logFile.append(string: filterredLog)
            } catch {
                debugPrint("Failed to append filterred log to log file")
            }
            
            do {
                let logFile = try self.logFileDest(fileName: "\(self.fileName)_states.csv")
                let statesLog = states.map{ String($0) }.joined(separator: "\n")
                try logFile.append(string: statesLog)
            } catch {
                debugPrint("Failed to append state log to log file")
            }
        }
    }
    
    @IBAction func sliderUpdateIntervalValueChanged(_ sender: Any) {
        guard let slider = sender as? UISlider else { return }
        let scale = slider.value
        MotionHelper.shared.updateInterval = TimeInterval(1 / scale)
        self.lblUpdateInterval.text = "Update interval: 1/\(scale) s"
    }
    
    @IBAction func btnProcessDataPressed(_ sender: Any) {
        DispatchQueue.global().async {
            DataProcessingHelper.processData(withCompletionHandler: {[weak self] (infoRef) in
                guard let infoRef = infoRef, let self = self else { return }
                DispatchQueue.main.async {
                    let realm = try! Realm()
                    guard let info = realm.resolve(infoRef) else {
                        return
                    }
                    self.renderBarCharts(info: info)
                }
            })
        }
    }
    
    private func renderBarCharts(info: SleepInfoModel) {
        self.viewCharts.drawBarShadowEnabled = false
        self.viewCharts.drawValueAboveBarEnabled = false
        self.viewCharts.legend.enabled = false
        
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([.hour, .minute], from: info.beginSleepTime!, to: info.endSleepTime!)
        
        let xAxisFormatter = SleepHourAxisValueFormatter()
        xAxisFormatter.beginTime = info.beginSleepTime
        
        let xAxis = self.viewCharts.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.labelCount = dateComponents.hour!
        xAxis.valueFormatter = xAxisFormatter
        xAxis.granularityEnabled = false
        
        let leftAxis = self.viewCharts.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 4
        leftAxis.labelPosition = .outsideChart
        leftAxis.granularity = 1
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0
        leftAxis.valueFormatter = SleepStateAxisValueFormater()
        
        self.viewCharts.rightAxis.enabled = false
        
        var barColors = [UIColor]()
        let yVals = info.sleepStates.enumerated().map { (index, state) -> BarChartDataEntry in
            switch state {
            case 1:
                barColors.append(UIColor.green)
                break
            case 2:
                barColors.append(UIColor.yellow)
                break
            default:
                barColors.append(UIColor.red)
                break
            }
            return BarChartDataEntry(x: Double(index), y: Double(state))
        }
        
        let dataSet : BarChartDataSet = BarChartDataSet(values: yVals, label: nil)
        dataSet.drawValuesEnabled = false
        dataSet.colors = barColors
        
        let data = BarChartData(dataSet: dataSet)
        data.barWidth = 0.5
        self.viewCharts.data = data
        self.viewCharts.data?.notifyDataChanged()
        self.viewCharts.notifyDataSetChanged()
        
    }
    
    private func processData(atIndex index: Int) {
        guard index < self.filterredData.count else { return }
        let result = DataProcessingHelper.medianFilter(paddedData: self.data, windowSize: windowSize, index: index)
        self.filterredData[index] = result
        self.filterredFlags.add(index)
    }
    
    private func logFileDest(fileName: String) throws -> File {
        let system = FileSystem()
        let path = system.homeFolder.path
        let folder = try! Folder(path: path)
        let documents = try! folder.subfolder(named: "Documents")
        let logFolder = try documents.createSubfolderIfNeeded(withName: "logs")
        return try logFolder.createFile(named: fileName)
    }
}

class SleepStateAxisValueFormater : IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let intVal = Int(value)
        switch intVal {
        case 1:
            return "Deep Sleep"
        case 2:
            return "Light Sleep"
        case 3:
            return "Wake"
        default:
            return ""
        }
    }
}

class SleepHourAxisValueFormatter : IAxisValueFormatter {
    
    var beginTime : Date?
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let curTZ = TimeZone.current
        let timeZoneOffset = curTZ.secondsFromGMT()
        let sleepTime = beginTime!.addingTimeInterval(value * 240 + TimeInterval(timeZoneOffset))
        return dateFormatter.string(from: sleepTime)
    }
}
